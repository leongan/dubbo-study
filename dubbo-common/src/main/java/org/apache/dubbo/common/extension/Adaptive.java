/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dubbo.common.extension;

import org.apache.dubbo.common.URL;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provide helpful information for {@link ExtensionLoader} to inject dependency extension instance.
 *
 * 自适应扩展点注解，可以用在方法上或者类上。
 * 用在方法上：
 *      Dubbo 初始化扩展点时会使用 Javassist 或者 JDK 代理技术来创建一个代理类（如 Transporter 接口的
 *      代理类 Transporter$Adaptive )。该方法会被实现一些通用的逻辑，即从 URL 中找到需要的信息调用真正
 *      的扩展实现类，是一种典型的「动态代理」模式。
 *      比如 Protocol 扩展，其 export() 和 refer() 方法标注了 @Adaptive。生成的 Protocol@Adaptive
 *      代理类就会实现这两个方法，从 URL 中提取配置的协议信息，然后通过 ExtensionLoader 拿到具体的扩展实现。
 * 用在类上：
 *      Adaptive 标注的类不需要创建代理类，如 {@link AdaptiveExtensionFactory} 和 {@link AdaptiveCompiler}，
 *      他们是对具体实现的抽象。也就是说 Dubbo 在初始化不需要明确指定要使用他们的哪个实现，而是交由这个抽象层来间接决定。
 *      同一个扩展点，类级别的 Adaptive 只能有一个！
 *
 * 简单来说，如果一个扩展接口的实现中没有一个使用了类上的 @Adaptive，那么就有 Dubbo 动态给这个接口生成一个代理类，在被标注
 * 了 @Adaptive 的方法里做适配。反之，如果扩展接口具备一个实现有类上的 @Adaptive，就表明适配逻辑由这个类来完成，Dubbo 不
 * 需要生成代理类。
 *
 * @see ExtensionLoader
 * @see URL
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Adaptive {
    /**
     * Decide which target extension to be injected. The name of the target extension is decided by the parameter passed
     * in the URL, and the parameter names are given by this method.
     * <p>
     * If the specified parameters are not found from {@link URL}, then the default extension will be used for
     * dependency injection (specified in its interface's {@link SPI}).
     * <p>
     * For example, given <code>String[] {"key1", "key2"}</code>:
     * <ol>
     * <li>find parameter 'key1' in URL, use its value as the extension's name</li>
     * <li>try 'key2' for extension's name if 'key1' is not found (or its value is empty) in URL</li>
     * <li>use default extension if 'key2' doesn't exist either</li>
     * <li>otherwise, throw {@link IllegalStateException}</li>
     * </ol>
     * If the parameter names are empty, then a default parameter name is generated from interface's
     * class name with the rule: divide classname from capital char into several parts, and separate the parts with
     * dot '.', for example, for {@code org.apache.dubbo.xxx.YyyInvokerWrapper}, the generated name is
     * <code>String[] {"yyy.invoker.wrapper"}</code>.
     *
     * @return parameter names in URL
     */
    String[] value() default {};

}